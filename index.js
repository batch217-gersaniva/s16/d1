console.log("Hello World!")

// [SECTION] Arithmetic Operators
// + , - , * , / , %

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of subtraction operator: " + product);

let quotient = x / y;
console.log("Result of subtraction operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// [SECTION] Assignment Operator
// Basic Assignment operator is (=) equal sign.

let assignmentNumber = 8;

// Addition Assignment
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber)

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber)


// Subtraction / Mult
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber)

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber)

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber)

// Multiple Operators and Parentheses


let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);

// Incrementation and Decrementation
// Operators that add or subtract values by 1 and reaasigns

let z = 1;

// Pre-incrementation
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//Post-incrementation
increment = z++;
console.log("Result of post-increment: " + increment);

// 
// Pre-decrementation
decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrementation
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//[SECTION] Type Coersion
// Type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let noncoercion = numC + numD;
console.log(noncoercion);
console.log(typeof noncoercion);


let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);


// [SECTION] Comparison Operators
let juan = "juan";

// Equality operator (==)
// Single equal (=) - assignment of value
// Double equal (==) - comparison equality

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');  
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);


// Inequality Operator (!=)
// !=not

console.log(1 != 1);
console.log(1 != 2);

// Strict Equality Operator
// Check if the value of the operand are equal and of the same type


console.log(1 === '1'); 
console.log("juan" === juan);
console.log(0 === false);

// Strict Inequality Operator

console.log(1 !== '1'); 
console.log("juan" !== juan);
console.log(0 !== false);

// Relational Operators
// Some comparison operators check whether one value is greater or less than to the other value
//  > and <

let a = 50;
let b = 64;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// [SECTION] Logical Operators
//  && --> AND, || --> OR, ! --> NOT

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical || Result: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Logical NOT Result: " + someRequirementsNotMet);




